This is a Scrapy project to scrape information about products at https://zakaz.atbmarket.com/

This project is only meant for educational purposes.
<h2>Extracted data</h2>
This project extracts all data including title, price, product_url etc...


<br>A sample item:
<pre><code>
{
    "product_url": "https://zakaz.atbmarket.com/product/900/9",
    "title": "Напій з екстрактом кави 20г MacCoffee Original (3 в 1) швидкорозчинний",
    "price": "320",
    "old_price": "320",
    "img_url": "https://src.zakaz.atbmarket.com/cache/photos/catalog_product_gal_mob_1835.jpg",
    "brand": "MacCoffee",
    "categories": "Бакалія;Кава, какао",
    "categories_ids": "13;215"
}
</code></pre>

<h2>Spider</h2>

This project contains spider: AtbSpider.

You can learn more about web scraping with Scrapy by going through the <a href="https://doc.scrapy.org/en/latest/intro/tutorial.html">original Scrapy Tutorial</a>.


To scrape a new website for locations, you'll want to create a new spider. You can copy from existing spiders or start from a blank, but the result is always a Python class that has a `process()` function that `yield`s [`GeojsonPointItem`s](https://github.com/iandees/all-the-places/blob/master/locations/items.py). The Scrapy framework does the work of outputting the GeoJSON based on these objects that the spider generates.

<h2>Virtual environments</h2>

To get started, you'll want to install the dependencies for this project.

1. This project uses `pipenv` to handle dependencies and virtual environments. To get started, make sure you have [`pipenv` installed](https://github.com/kennethreitz/pipenv#installation).

1. With `pipenv` installed, make sure you have the `products-scraper` repository checked out

   ```
   git clone git@gitlab.com:andriy.shevchuk/products-scraper.git
   ```

1. Then you can install the dependencies for the project

   ```
   cd products-scraper
   pipenv install
   ```

1. After dependencies are installed, make sure you can run the `scrapy` command without error

   ```
   pipenv run scrapy
   ```

1. If `pipenv run scrapy` ran without complaining, then you have a functional `scrapy` setup and are ready to start spider.

<h2>Running the spider</h2>

You can run a spider using the <i>scrapy crawl</i> command:

If you want to save the scraped data to a file, you can pass the `-o` option:

<h4>Save the scraped data to json file:</h4>
<pre><code>
    cd atb_products
    scrapy crawl AtbSpider -O products.json  
</code></pre>

<h4>Save the scraped data to csv file:</h4>

<pre><code>
    scrapy crawl AtbSpider -O products.csv 
</code></pre>