# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class AtbProductsItem(scrapy.Item):
    product_url = scrapy.Field()
    title = scrapy.Field()
    img_url = scrapy.Field()
    price = scrapy.Field()
    old_price = scrapy.Field()
    brand = scrapy.Field()
    categories = scrapy.Field()
    categories_ids = scrapy.Field()
