import re


class AtbProductsPipeline:
    def process_item(self, item, spider):
        item['title'] = item['title'].replace(u'\xa0', u' ')
        if len(item['price']) > 1:
            item['old_price'] = item['price'][0]
            item['price'] = item['price'][1]
            item['old_price'] = ''.join(re.findall(r'\d+', item['old_price']))
        else:
            item['price'] = item['price'][0]
            item['old_price'] = '0'

        item['categories'] = ';'.join(item['categories'][1:])
        item['categories_ids'] = ';'.join(list(map(lambda x: x.split('/')[-1], item['categories_ids']))[1:])
        item['price'] = ''.join(re.findall(r'\d+', item['price']))

        return item
