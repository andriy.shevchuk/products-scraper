from urllib.parse import urljoin

import scrapy
from ..items import AtbProductsItem


class AtbSpider(scrapy.Spider):
    name = 'AtbSpider'
    allowed_domains = ['zakaz.atbmarket.com']

    def __init__(self, location_id=900, *args, **kwargs):
        super(AtbSpider, self).__init__(*args, **kwargs)
        self.start_urls = [f'https://zakaz.atbmarket.com/catalog/{location_id}']

    # start_urls = ['https://zakaz.atbmarket.com/catalog/900']
    visited_urls = []

    def parse(self, response):
        if response.url not in self.visited_urls:
            self.visited_urls.append(response.url)
            for product_link in response.css('.product-detail a::attr(href)').getall():
                url = f"https://zakaz.atbmarket.com{product_link}"
                yield response.follow(url, callback=self.parse_product)

            next_page = response.xpath(
                '//li[contains(@class, "page-item") and'
                ' not(contains(@class, "active"))]/a/@href').extract()[-1]

            next_page_url = urljoin(response.url + '/', next_page)
            yield response.follow(next_page_url, callback=self.parse)

    @staticmethod
    def parse_product(response):
        item = AtbProductsItem()
        item['product_url'] = response.url
        item['title'] = response.css('h1::text').get()
        item['price'] = response.css(".price").getall()
        item['old_price'] = response.css(".price").getall()
        item['img_url'] = response.css('.product-label img::attr(src)').get()
        item['brand'] = response.css('.collection-brand-filter .category-list li a::text').get()
        item['categories'] = response.css('.breadcrumb .breadcrumb-item a::text').getall()
        item['categories_ids'] = response.css('.breadcrumb .breadcrumb-item a::attr(href)').getall()
        return item
